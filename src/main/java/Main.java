import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Main class application. Initiate computing and print result.
 */
public class Main {
    public static void main(String[] args) {
        ApacheSQL16 apacheSQL16 = new ApacheSQL16();
        apacheSQL16.usersPath = args[0];
        Set<String> ipAddresses = new HashSet<>();
        for (int i = 1; i < args.length; i++) {
            ipAddresses.add(args[i]);
        }
        Map<String, List<Double>> res = apacheSQL16.init(ipAddresses);
        List<Double> salary = res.get(ApacheSQL16.KEY_SALARY);
        List<Double> trips = res.get(ApacheSQL16.KEY_TRIPS);

        System.out.println("Citizens statistics:");
        System.out.println(ApacheSQL16.AGE1 + "," + salary.get(0) + "," + trips.get(0));
        System.out.println(ApacheSQL16.AGE2 + "," + salary.get(1) + "," + trips.get(1));
        System.out.println(ApacheSQL16.AGE3 + "," + salary.get(2) + "," + trips.get(2));
        apacheSQL16.destroy();
    }
}
